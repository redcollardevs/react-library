import BezierEasing from "./bezier-easing.lib.js";

export interface BezierEasingList {
  [index: string]: (x: number) => number;
}

export const bezierEasings: BezierEasingList = {
  ease: BezierEasing(1, 0, 0.3, 1),
  easeOut: BezierEasing(0.165, 0.84, 0.44, 1),
  easeIn: BezierEasing(0.755, 0.05, 0.855, 0.06),
  easeOutStrong: BezierEasing(0.165, 0.84, 0.44, 1),
  easeInStrong: BezierEasing(0.755, 0.05, 0.855, 0.06),
  default: BezierEasing(0.25, 0.1, 0.25, 1),
  easeInOut2: BezierEasing(0.455, 0.03, 0.515, 0.955),
  easePower2: BezierEasing(0.77, 0, 0.175, 1),
  easePower3: BezierEasing(0.645, 0.045, 0.355, 1),
  easePower4: BezierEasing(0.86, 0, 0.07, 1),
  easeSine: BezierEasing(0.445, 0.05, 0.55, 0.95),
  easeInPower2: BezierEasing(0.55, 0.055, 0.675, 0.19),
  easeInPower3: BezierEasing(0.55, 0.085, 0.68, 0.53),
  easeOutPower2: BezierEasing(0.25, 0.46, 0.45, 0.94),
  easeOutPower3: BezierEasing(0.215, 0.61, 0.355, 1),
};

export interface CSSEasingList {
  [index: string]: string;
}

export const cssEasings: CSSEasingList = {
  ease: "cubic-bezier(1, 0, 0.3, 1)",
  easeOut: "cubic-bezier(0.165, 0.84, 0.44, 1)",
  easeIn: "cubic-bezier(0.755, 0.05, 0.855, 0.06)",
  easeOutStrong: "cubic-bezier(0.165, 0.84, 0.44, 1)",
  easeInStrong: "cubic-bezier(0.755, 0.05, 0.855, 0.06)",
  default: "cubic-bezier(0.25, 0.1, 0.25, 1)",
  easeInOut2: "cubic-bezier(0.455, 0.030, 0.515, 0.955)",
  easePower2: "cubic-bezier(0.77, 0, 0.175, 1)",
  easePower3: "cubic-bezier(0.645, 0.045, 0.355, 1)",
  easePower4: "cubic-bezier(0.86, 0, 0.07, 1)",
  easeSine: "cubic-bezier(0.445, 0.05, 0.55, 0.95)",
  easeInPower2: "cubic-bezier(0.55, 0.055, 0.675, 0.19)",
  easeInPower3: "cubic-bezier(0.55, 0.085, 0.68, 0.53)",
  easeOutPower2: "cubic-bezier(0.25, 0.46, 0.45, 0.94)",
  easeOutPower3: "cubic-bezier(0.215, 0.610, 0.355, 1)",
};
