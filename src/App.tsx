import styles from "./App.module.scss";
import {
  Slider,
  SliderControl,
  sliderAnimationTypes,
} from "./shared/slider";
import { Modal, ModalGroup, ModalControl } from "./shared/modal";
import { useScrollLock } from "./shared/scroll/useScrollLock";

import { Header } from "./entity/header/Header";

import {
  setUseWhatChange,
} from '@simbathesailor/use-what-changed';

export const App = () => {
  // For debugging
  setUseWhatChange(true);

  // initialize body locking
  useScrollLock(document.body);

  return (
    <div className={styles.PageWrapper}>
      <Header/>
      <main className={styles.Body}>
        <Slider id="test-slider" className={styles.Slider}>
          <div className={styles.Slider__Slide}>
            <div className={styles.Slider__Inner}>
              <p>Simple text slider with default props.</p>
              <div className={styles.BtnWrapper}>
                <ModalControl id="modal-1" className={styles.Button}>
                  Read more
                </ModalControl>
              </div>
            </div>
          </div>

          <div className={styles.Slider__Slide}>
            <div className={styles.Slider__Inner}>
              <p>Some text #2</p>
              <div className={styles.BtnWrapper}>
                <ModalControl id="modal-2" className={styles.Button}>
                  Read more
                </ModalControl>
              </div>
            </div>
          </div>

          <div className={styles.Slider__Slide}>
            <div className={styles.Slider__Inner}>
              <p>Some text #3</p>
              <div className={styles.BtnWrapper}>
                <ModalControl id="modal-3" className={styles.Button}>
                  Read more
                </ModalControl>
              </div>
            </div>
          </div>
        </Slider>

        <div className={styles.Slider__Controls}>
          <SliderControl
            id="test-slider"
            to="prev"
            className={styles.Button}
            disabledClassName={styles.Button_Disabled}
          >
            prev
          </SliderControl>
          <SliderControl
            id="test-slider"
            to={0}
            className={styles.Button}
            disabledClassName={styles.Button_Disabled}
          >
            1
          </SliderControl>
          <SliderControl
            id="test-slider"
            to={1}
            className={styles.Button}
            disabledClassName={styles.Button_Disabled}
          >
            2
          </SliderControl>
          <SliderControl
            id="test-slider"
            to={2}
            className={styles.Button}
            disabledClassName={styles.Button_Disabled}
          >
            3
          </SliderControl>
          <SliderControl
            id="test-slider"
            to="next"
            className={styles.Button}
            disabledClassName={styles.Button_Disabled}
          >
            next
          </SliderControl>
        </div>

        <Slider
          id="test-slider-2"
          continuous={false}
          className={styles.Slider}
          animation={sliderAnimationTypes.DEFAULT_ANIMATION}
        >
          <div className={styles.Slider__Slide}>
            <div className={styles.Slider__Inner}>Some text</div>
          </div>
          <div className={styles.Slider__Slide}>
            <div className={styles.Slider__Inner}>Some text</div>
          </div>
        </Slider>

        <section>
          <div className={styles.Wrapper}>
            <div className={`${styles.Text} ${styles.Text_Centered}`}>
              <p>
                The Yuan dynasty (Chinese: 元朝; pinyin: Yuán Cháo), officially
                the Great Yuan[5] (Chinese: 大元; pinyin: Dà Yuán; Middle
                Mongolian: ᠶᠡᠭᠡᠶᠤᠸᠠᠨᠤᠯᠤᠰ, Yeke Ywan Ulus, literally "Great Yuan
                State"[note 3]), was a successor state to the Mongol Empire
                after its division and a ruling dynasty of China established by
                Kublai Khan, leader of the Mongol Borjigin clan, lasting from
                1271 to 1368. In Chinese historiography, this dynasty followed
                the Song dynasty and preceded the Ming dynasty
              </p>
              <p>
                Although Genghis Khan (an uprising ex-subject of the Jin Empire)
                had been enthroned with the Chinese title of Emperor[note 2] in
                1206[2] and the Mongol Empire had ruled territories including
                modern-day northern China for decades, it was not until 1271
                that Kublai Khan officially proclaimed the dynasty in the
                traditional Chinese style,[6] and the conquest was not complete
                until 1279 when the Southern Song dynasty was defeated in the
                Battle of Yamen. His realm was, by this point, isolated from the
                other Mongol khanates and controlled most of modern-day China
                and its surrounding areas, including modern Mongolia.[7] It was
                the first non-Han dynasty to rule all of China proper and lasted
                until 1368 when the Ming dynasty defeated the Yuan forces.[8][9]
                Following that, the rebuked Genghisid rulers retreated to the
                Mongolian Plateau and continued to rule as the Northern Yuan
                dynasty.[10]
              </p>
              <p>
                Some of the Yuan emperors mastered the Chinese language, while
                others only used their native Mongolian language and the
                'Phags-pa script.[11]
              </p>
              <p>
                After the division of the Mongol Empire, the Yuan dynasty was
                the khanate ruled by the successors of Möngke Khan. In official
                Chinese histories, the Yuan dynasty bore the Mandate of Heaven.
                The dynasty was established by Kublai Khan, yet he placed his
                grandfather Genghis Khan on the imperial records as the official
                founder of the dynasty and accorded him the temple name
                Taizu.[note 2] In the edict titled Proclamation of the Dynastic
                Name,[3] Kublai announced the name of the new dynasty as Great
                Yuan and claimed the succession of former Chinese dynasties from
                the Three Sovereigns and Five Emperors to the Tang dynasty.[3]
              </p>
            </div>
          </div>
        </section>

        <ModalGroup>
          <Modal
            className={styles.Modal}
            id="menu"
            activeClassName={styles.Modal_Active}
          >
            <div className={styles.Modal__Overflow}>
              <div className={styles.Modal__Body}>
                <div className={`${styles.Text} ${styles.Text_Centered}`}>
                  <h2>This is supposed to be a menu</h2>
                  <p>Nothing here for now</p>
                </div>
              </div>
            </div>
          </Modal>

          <Modal
            className={styles.Modal}
            id="modal-1"
            activeClassName={styles.Modal_Active}
          >
            <div className={styles.Modal__Overflow}>
              <div className={styles.Modal__Body}>
                <div className={`${styles.Text} ${styles.Text_Centered}`}>
                  <h2>Modal example</h2>
                  <p>Fullscreen modal width some text and default animation</p>
                  <p><ModalControl id="menu" className={styles.Button}>Button in modal example</ModalControl></p>
                </div>
              </div>
            </div>
          </Modal>
        </ModalGroup>
      </main>
    </div>
  );
}