import React, { useCallback, useEffect, useLayoutEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../redux/store";
import { modalDefaultProps } from "./defaults";
import { animationTypes } from "./Modal/animations/types";
import { useScrollLock } from "../scroll/useScrollLock";
import { getFocusable } from "../utils/dom.utils";
import { useFocusLock } from "../focus/useFocusLock";
import { OffsetScrollbar } from "../scroll/types";

export interface ModalProps {
  id: string;
  groupId?: string;
  className?: string;
  activeClassName?: string;
  style?: React.CSSProperties;
  children?: React.ReactElement[] | React.ReactElement | undefined | null;
  animation?: string;
  focusLock?: boolean;
  offsetScrollbar?: OffsetScrollbar;
}

export const Modal = ({
  id,
  groupId = modalDefaultProps.groupId,
  className,
  activeClassName = modalDefaultProps.activeClassName,
  style = {},
  children,
  animation = modalDefaultProps.animation,
  focusLock = modalDefaultProps.focusLock,
  offsetScrollbar = modalDefaultProps.offsetScrollbar,
}: ModalProps) => {
  const modalState = useSelector((state: RootState) =>
    state.modal ? state.modal.groups[groupId] : undefined
  );
  const [classNames, setClassNames] = useState<Array<string>>([]);

  const stateActive = modalState?.active.includes(id);
  const active = useRef(false);
  const wrapperElement = useRef<HTMLDivElement | null>(null);
  const focusLockHook = useFocusLock(wrapperElement);

  const lockFocus = focusLockHook.lock;
  const unlockFocus = focusLockHook.unlock;

  const enableFocus = useCallback(() => {
    if (!wrapperElement.current) return;
    getFocusable(wrapperElement.current).forEach((el) => {
      el.setAttribute("tablindex", "0");
    });

    if (focusLock) {
      lockFocus();
    }
  }, [focusLock, lockFocus]);

  const disableFocus = useCallback(() => {
    if (!wrapperElement.current) return;
    getFocusable(wrapperElement.current).forEach((el) => {
      el.setAttribute("tablindex", "-1");
    });

    if (focusLock) {
      unlockFocus();
    }
  }, [focusLock, unlockFocus]);

  useEffect(() => {
    if (!wrapperElement.current) return;

    getFocusable(wrapperElement.current).forEach((el) => {
      el.setAttribute("tablindex", "-1");
    });
  }, []);

  useEffect(() => {
    setClassNames((prevClassNames) => {
      return className
        ? prevClassNames.includes(className)
          ? prevClassNames
          : [...prevClassNames, className]
        : prevClassNames;
    });
  }, [className]);

  useLayoutEffect(() => {
    if (stateActive && !active.current) {
      active.current = true;

      setTimeout(() => {
        enableFocus();

        setClassNames((prevClassNames) => {
          return prevClassNames.includes(activeClassName)
            ? prevClassNames
            : [...prevClassNames, activeClassName];
        });
      }, 1000 / 60);
    } else if (!stateActive && active.current) {
      active.current = false;

      setClassNames((prevClassNames) => {
        disableFocus();

        return prevClassNames.includes(activeClassName)
          ? prevClassNames.filter((cl) => {
              return cl !== activeClassName;
            })
          : prevClassNames;
      });
    }
  }, [enableFocus, disableFocus, stateActive, activeClassName]);

  const animationResult = animationTypes[animation].useAnimation(
    id,
    groupId,
    wrapperElement
  );

  const scrollLock = useScrollLock();
  if (offsetScrollbar === "right") {
    style.right = scrollLock.offset;
  } else if (offsetScrollbar === "paddingRight") {
    style.paddingRight = scrollLock.offset;
  } else if (offsetScrollbar === "marginRight") {
    style.marginRight = scrollLock.offset;
  }

  return animationResult.visible ? (
    <div ref={wrapperElement} className={classNames.join(" ")} style={style}>
      {children}
    </div>
  ) : null;
};
