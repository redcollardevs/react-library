import { ModalProps } from "./Modal";
import { ModalControlProps, ControlActions } from "./ModalControl";
import { ModalGroupProps } from "./ModalGroup";
import { DEFAULT_GROUP_ID, Modes } from "./types";
import { animationTypes } from "./Modal/animations/types";
import { OffsetScrollbar } from "../scroll/types";

interface ModalDefaultProps extends ModalProps {
  id: string;
  groupId: string;
  activeClassName: string;
  animation: string;
  focusLock: boolean;
  offsetScrollbar: OffsetScrollbar;
}

export const modalDefaultProps: ModalDefaultProps = {
  id: "",
  groupId: DEFAULT_GROUP_ID,
  activeClassName: "active",
  animation: animationTypes.DEFAULT_ANIMATION.ANIMATION_NAME,
  focusLock: true,
  offsetScrollbar: "right",
};

interface ModalGroupDefaultProps extends ModalGroupProps {
  id: string;
  mode: Modes;
  scrollLock: boolean;
  tabLock: boolean;
  offsetScrollbar: OffsetScrollbar;
}

export const modalGroupDefaultProps: ModalGroupDefaultProps = {
  id: DEFAULT_GROUP_ID,
  mode: "replace",
  scrollLock: true,
  tabLock: true,
  children: [],
  offsetScrollbar: "right",
};

interface ModalControlDefaultProps extends ModalControlProps {
  mode: ControlActions;
  activeClassName: string;
  alwaysFocusable: boolean;
}

export const modalControlDefaultProps: ModalControlDefaultProps = {
  id: "",
  mode: "toggle",
  activeClassName: "active",
  alwaysFocusable: false,
};
