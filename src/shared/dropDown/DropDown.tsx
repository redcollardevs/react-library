// <drop-down>
// 		<button class="button">header</button>
//		<div class="outer">
//			<div class="inner">
//				.......
//				.......
//			</div>
//		</div>
// </drop-down>

import { useRef, useState } from "react";

// drop-down .outer {
//		overflow: hidden;
//		height: 0;
// }

interface DropDownProps {
  headerText?: string;
  closeOutside?: boolean;
  openDuration?: number;
  closeDuration?: number;
  closeButton?: React.ReactElement;
  children?: React.ReactElement[] | React.ReactElement | undefined | null;
}

export const DropDown = ({
  headerText = "",
  closeOutside = false,
  openDuration = 0.3,
  closeDuration = 0.35,
  closeButton,
  children,
}: DropDownProps) => {
  const [active, setActive] = useState(false);

  const outerRef = useRef(null);
  const innerRef = useRef(null);

  const [attributes, setAttributes] = useState({
    ariaHaspopup: true,
    ariaExpanded: false,
  });

  const onFocus = () => {};

  const onBlur = () => {};

  const onClick = () => {};

  return (
    <div>
      <button
        onFocus={onFocus}
        onBlur={onBlur}
        onClick={onClick}
        aria-hasPopup={attributes.ariaHaspopup}
        aria-expanded={attributes.ariaExpanded}
      >
        {headerText}
      </button>
      {closeButton}
      <div ref={outerRef}>
        <div ref={innerRef}>{children}</div>
      </div>
    </div>
  );
};
