import { useRef, useMemo, useCallback } from "react";
import { useElementResize } from "../resize/useElementResize";
import { RootState } from "../../redux/store";
import { useScroll } from "./useScroll";
import { useDispatch, useSelector } from "react-redux";
import {
  scrollLock,
  scrollUnlock,
} from "../../redux/reducers/scrollReducer/actions";

interface UseLockData {
  /**
   * Locks the scroll
   */
  lock: VoidFunction;
  /**
   * Unlocks the scroll
   */
  unlock: VoidFunction;
  /**
   * Scrollbar offset value (just set it to styles.right)
   */
  offset: number;
  // counter: number
}

interface UseScrollLock {
  (element?: HTMLElement | null): UseLockData;
}

/**
 * It's a global value, so only the first call will lock the body
 */
let bodyLocked = false;
/**
 * How much the body size was change. We need to store it globaly so calls with "referenceBody" flag will know what value to reference.
 */
let bodyMargin = 0;
/**
 * How much body was shifted to mimic the scroll position. We store it globally since any hook call can unlock the body and this value is needed to set the correct scroll position.
 */
let bodyShift = 0;

/**
 * Provides methods to disable scrolling, automaticly works with body and returns body offset value in other cases so it can be used with fixed elements
 * To check if body is locked please use the scroll store.
 * @param element Use global wrapper (document.body, probably) to initialize scroll locking or leave it empty
 * @returns Methods and current body offset
 */
export const useScrollLock: UseScrollLock = (element) => {
  const dispatch = useDispatch();
  const offset = useRef(0);

  const scrollLockState = !!useSelector((state: RootState) =>
    state.scroll ? state.scroll.locked : undefined
  );

  const elementScrollbarFixed = useRef(false);
  const scroll = useScroll();

  /**
   * Get element size before the body is locked, so we can access the previous value of it's size
   */
  const elementSize = useElementResize(element || null);

  /**
   * Disable Scroll
   */
  const lock = useCallback(() => {
    if (bodyLocked) return;
    bodyLocked = true;

    const body = document.body;

    body.style.position = "fixed";
    body.style.overflow = "hidden";
    body.style.left = "0px";
    body.style.right = "0px";

    const scrolled = scroll.get().top;

    bodyShift = scrolled;
    body.style.top = -scrolled + "px";

    window.scrollTo(0, 0);

    dispatch(scrollLock());
  }, [dispatch, scroll]);

  /**
   * Enable Scroll
   */
  const unlock = useCallback(() => {
    if (!bodyLocked) return;
    bodyLocked = false;

    const body = document.body;

    body.style.position = "";
    body.style.overflow = "";
    body.style.left = "";
    body.style.right = "";
    body.style.top = "";

    window.scrollTo(0, bodyShift);

    dispatch(scrollUnlock());
  }, [dispatch]);

  if (scrollLockState && !elementScrollbarFixed.current) {
    let diff: number;

    if (element === document.body) {
      element.style.right = "0px";
      diff = element.clientWidth - elementSize.get().width;

      bodyMargin = diff;

      element.style.right = `${Math.max(0, diff)}px`;
    } else {
      diff = bodyMargin;
    }

    offset.current = diff;

    elementScrollbarFixed.current = true;
  } else if (!scrollLockState && elementScrollbarFixed.current) {
    if (element === document.body) {
      element.style.right = "";
    }

    offset.current = 0;
    elementScrollbarFixed.current = false;
  }

  return {
    lock: lock,
    unlock: unlock,
    offset: offset.current,
  };
};
