import { useState, useEffect, useMemo, useCallback, useRef } from "react";

export interface ElementSize {
  width: number;
  height: number;
};

export interface Resize extends ElementSize {
  get: () => ElementSize
};

export interface UseResize {
  (): Resize;
}

/**
 * Returns the viewport size & getter function.
 * @returns Viewport size
 */
export const useResize:UseResize = () => {
  const sizeRef = useRef<ElementSize>({
    width: 0,
    height: 0,
  });
  const [size, setState] = useState(sizeRef.current);

  const onResize = useCallback(() => {
    sizeRef.current.width = window.innerWidth;
    sizeRef.current.height = window.innerHeight;

    setState(sizeRef.current);
  }, []);

  const get = useCallback(() => {
    return sizeRef.current;
  }, []);

  useEffect(() => {
    window.addEventListener("resize", onResize, { passive: true });
    window.addEventListener("orientationchange", onResize, { passive: true });
    window.addEventListener("load", onResize, { passive: true });

    onResize();

    return () => {
      window.removeEventListener("resize", onResize);
      window.removeEventListener("orientationchange", onResize);
      window.removeEventListener("load", onResize);
    };
  }, [onResize]);

  return useMemo(() => {
    return {
      width: size.width,
      height: size.height,
      get: get
    };
  }, [size.width, size.height, get]);
};
