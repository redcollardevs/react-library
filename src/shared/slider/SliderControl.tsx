import React, { useCallback } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/store";
import {
  sliderPrev,
  sliderNext,
  sliderTo,
} from "../../redux/reducers/sliderReducer/actions";

type To = "next" | "prev" | number;

export interface SliderControlProps {
  id: string;
  className?: string;
  to: To;
  disabledClassName?: string;
  activeClassName?: string;
  children?: React.ReactElement[] | React.ReactElement | string;
}

/**
 * Control buttons for slider
 */
const SliderControl = (props: SliderControlProps) => {
  const dispatch = useDispatch();
  const sliderState = useSelector((state: RootState) => state.slider);

  const handleClick = useCallback(() => {
    if (props.to === "next") {
      dispatch(sliderNext(props.id));
    } else if (props.to === "prev") {
      dispatch(sliderPrev(props.id));
    } else {
      dispatch(sliderTo(props.id, props.to, 0));
    }
  }, [dispatch, props.id, props.to]);

  const sliderStateData = sliderState?.items.find(
    (item) => item.id === props.id
  );

  let ariaDisabled: boolean = false;
  let classNames = [];

  if (props.className) {
    classNames.push(props.className);
  }

  const disabledClassName = props.disabledClassName || "disabled";
  const activeClassName = props.activeClassName || "active";

  if (sliderStateData) {
    if (props.to === "next" || props.to === "prev") {
      if (
        !sliderStateData.continuous &&
        props.to === "next" &&
        sliderStateData.index === sliderStateData.total - 1
      ) {
        classNames.push(disabledClassName);
        ariaDisabled = true;
      } else if (
        !sliderStateData.continuous &&
        props.to === "prev" &&
        sliderStateData.index === 0
      ) {
        classNames.push(disabledClassName);
        ariaDisabled = true;
      }
    } else {
      if (props.to === sliderStateData.index) {
        classNames.push(disabledClassName);
        classNames.push(activeClassName);
        ariaDisabled = true;
      }
    }
  }

  return (
    <button
      className={classNames.join(" ")}
      aria-disabled={ariaDisabled}
      onClick={handleClick}
    >
      {props.children}
    </button>
  );
};

export default SliderControl;
