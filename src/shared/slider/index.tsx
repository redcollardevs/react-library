import Slider from "./Slider";
import SliderControl from "./SliderControl";
import { animationTypes } from "./Slider/animations/types"

const sliderAnimationTypes = animationTypes;

export { Slider, SliderControl, sliderAnimationTypes };
