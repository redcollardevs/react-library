import React from "react";

import { sliderDefaultProps } from "./defaults";
import { SliderCore } from "./Slider/common/SliderCore"

export interface SliderProps {
  id: string;
  speed?: number; // animation speed
  continuous?: boolean; // the slider is continuous
  focusable?: boolean; // can be focused and controled with arrows from the keyboard
  touch?: boolean; // can be controled by swipes on a touch devise
  draggable?: boolean; // can be dragged by mouse
  resize?: boolean; // autoresize
  animation?: string; // animation name
  leftSlideClassName?: string; // className of slides to the left of the center
  rightSlideClassName?: string; // className of slides to the rigth of the center
  centerSlideClassName?: string; // className of slide in the center
  className?: string;
  style?: React.CSSProperties;
  children?: React.ReactElement[];
}

export interface SliderState {
  index: number;
  total: number;
  direction: string;
  topZIndex: number;
  animationMode: string;
  animationName: string;
}

/**
 * React slider component
 */
const Slider = ({
  id,
  speed = sliderDefaultProps.speed,
  continuous = sliderDefaultProps.continuous,
  focusable = sliderDefaultProps.focusable,
  touch = sliderDefaultProps.touch,
  draggable = sliderDefaultProps.draggable,
  resize = sliderDefaultProps.resize,
  animation = sliderDefaultProps.animation,
  leftSlideClassName = sliderDefaultProps.leftSlideClassName,
  rightSlideClassName = sliderDefaultProps.rightSlideClassName,
  centerSlideClassName = sliderDefaultProps.centerSlideClassName,
  className,
  style,
  children
}:SliderProps) => {
  return (
    <SliderCore 
      id={id}
      speed={speed}
      continuous={continuous}
      focusable={focusable}
      touch={touch}
      draggable={draggable}
      resize={resize}
      animation={animation}
      leftSlideClassName={leftSlideClassName}
      rightSlideClassName={rightSlideClassName}
      centerSlideClassName={centerSlideClassName}
      className={className}
      style={style}>
      {children}
    </SliderCore>
  );
};

Slider.defaultProps = sliderDefaultProps;

export default Slider;