import { forwardRef } from "react";
import type { StylesAnimation } from "../animations/interface";
import type { StylesDrag } from "../controls/useDrag";

interface SliderSlideProps {
  className?: string;
  style?: React.CSSProperties;
  children?: React.ReactElement[];
  key: string;
  stylesAnimation: StylesAnimation;
  stylesDrag: StylesDrag;
}

interface Styles {
  willChange?: string;
  transitionProperty?: string;
  transitionDuration?: number;
  transitionDelay?: number;
  transitionTimingFunction?: string;
  transform?: number;
  opacity?: number;
  zIndex?: number;
}

/**
 * Merges styles, sums or multiplyes numeric styles, for non-numeric the second argument takes priority
 * @param s1 First styles object
 * @param s2 Second styles object
 * @returns Merged styles
 */
const mergeStyles = (s1: Styles, s2: Styles): Styles => {
  let transform;
  if (s1.transform && s2.transform) {
    transform = s1.transform + s2.transform;
  } else if (s1.transform) {
    transform = s1.transform;
  } else if (s2.transform) {
    transform = s2.transform;
  } else {
    transform = 0;
  }

  let opacity;
  if (s1.opacity && s2.opacity) {
    opacity = s1.opacity * s2.opacity;
  } else if (s1.opacity) {
    opacity = s1.opacity;
  } else if (s2.opacity) {
    opacity = s2.opacity;
  } else {
    opacity = 0;
  }

  return {
    willChange: s2.willChange || s1.willChange,
    transitionProperty:
      s2.transitionProperty || s1.transitionProperty || "none",
    transitionDuration: s2.transitionDuration || s1.transitionDuration || 0,
    transitionDelay: s2.transitionDelay || s1.transitionDelay || 0,
    transitionTimingFunction:
      s2.transitionTimingFunction || s1.transitionTimingFunction || "ease",
    transform: transform,
    opacity: opacity,
    zIndex: s2.zIndex || s1.zIndex || 0,
  };
};

/**
 * Slider inner slide component
 */
const Slide: React.ForwardRefRenderFunction<HTMLDivElement, SliderSlideProps> =
  (props, ref) => {
    const renderInnerSLide = (children: React.ReactElement[] | undefined) => {
      return children;
    };

    const basicStyles: React.CSSProperties = {
      position: "absolute",
      left: 0,
      top: 0,
      width: "100%",
      height: "100%",
      overflow: "hidden",
    };

    let mergedStyles: Styles = {
      willChange: "auto",
      transitionProperty: "none",
      transitionDuration: 0,
      transitionDelay: 0,
      transitionTimingFunction: "ease",
      transform: 0,
      opacity: 1,
      zIndex: 0,
    };

    if (props.stylesAnimation) {
      mergedStyles = mergeStyles(mergedStyles, props.stylesAnimation);
    }

    if (props.stylesDrag) {
      mergedStyles = mergeStyles(mergedStyles, props.stylesDrag);
    }

    const styles: React.CSSProperties = {
      ...basicStyles,
      ...props.style,
      transform: `translateX(${mergedStyles.transform}px)`,
      opacity: mergedStyles.opacity,
      willChange: mergedStyles.willChange,
      transitionProperty: mergedStyles.transitionProperty,
      transitionDuration: `${mergedStyles.transitionDuration}s`,
      transitionDelay: `${mergedStyles.transitionDelay}s`,
      transitionTimingFunction: mergedStyles.transitionTimingFunction,
    };

    return (
      <div className={props.className} style={styles} ref={ref}>
        {renderInnerSLide(props.children)}
      </div>
    );
  };

export const SliderSlide = forwardRef(Slide);
