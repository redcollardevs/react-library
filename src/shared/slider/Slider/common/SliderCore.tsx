import * as React from "react";

import { Children, useState, useEffect, useRef } from "react";
import { SliderSlide } from "./SliderSlide";
import type { RootState } from "../../../../redux/store";
import { sliderDefaultState } from "../../defaults";
import { useDispatch, useSelector } from "react-redux";
import { useResize } from "../../../resize/useResize";
import { sliderUpdate } from "../../../../redux/reducers/sliderReducer/actions";
import { animationList } from "../animations/animationsList";
import { useDrag } from "../controls/useDrag";
import { useFocus } from "../controls/useFocus";
import { useTab } from "../controls/useTab";

// TODO: Set zIndex of next slide

import type { SliderProps } from "../../Slider";
import { DEFAULT_ANIMATION } from "../animations/types";

export interface SliderCoreProps extends SliderProps{
  speed: number;
  continuous: boolean;
  focusable: boolean;
  touch: boolean;
  draggable: boolean;
  resize: boolean;
  leftSlideClassName: string;
  rightSlideClassName: string;
  centerSlideClassName: string;
  animation: string;
}

/**
 * React slider core component
 */
export const SliderCore = (props:SliderCoreProps) => {
  const animationData = animationList[props.animation || DEFAULT_ANIMATION];
  const [state, setState] = useState({
    ...sliderDefaultState,
    animationMode: animationData.mode,
    animationName: animationData.name,
  });

  const sliderStoreData = useSelector((state: RootState) => state.slider);
  const sliderState = sliderStoreData?.items.find(
    (item) => item.id === props.id
  );

  const containerRef = useRef<HTMLDivElement>(null);
  const slideRefs: React.RefObject<HTMLDivElement>[] = [];
  const dispatch = useDispatch();

  useResize();

  const total = Children.count(props.children);

  useEffect(() => {
    if (total > 0) {
      let topZ = 0;

      setState((prevState) => ({
        ...prevState,
        index: 0,
        total: total,
      }));

      setState((prevState) => ({ ...prevState, topZIndex: topZ }));
    } else {
      setState((prevState) => ({
        ...prevState,
        index: 0,
        total: 0,
      }));
    }

    dispatch(sliderUpdate(props.id, state.total, 0, props.continuous, 0));
  }, [total, props.continuous, props.id, state.total, dispatch]);

  useEffect(() => {
    setState((prevState) => {
      if (!sliderState || sliderState.index === prevState.index) {
        return prevState
      } else {
        return {
          ...prevState,
          index: sliderState.index,
          topZIndex: prevState.topZIndex + 1,
        };
      }
    });
  }, [sliderState, sliderState?.index]);

  const stylesAnimation = animationList[
    props.animation || DEFAULT_ANIMATION
  ].useAnimation(state, containerRef.current, props);

  const drag = useDrag(state, containerRef.current, props);

  const slides = props.children
    ? props.children.map((ch, index) => {
        const ref = React.createRef<HTMLDivElement>();
        slideRefs.push(ref);

        const previousClass = state.total > 2 ? props.leftSlideClassName : null;
        const nextClass = state.total > 2 ? props.leftSlideClassName : null;
        const currentClass = props.centerSlideClassName;

        const positionClassName = index < state.index ? previousClass : index > state.index ? nextClass : currentClass;
        const classNames = positionClassName ? [positionClassName, ch.props.className] : [ch.props.className];

        return (
          <SliderSlide
            ref={ref}
            key={ch.props.key ? ch.props.key : index}
            stylesAnimation={stylesAnimation[index]}
            stylesDrag={drag.slideStyles[index]}
            style={ch.props.style}
            className={classNames.join(" ")}
          >
            {ch.props.children}
          </SliderSlide>
        );
      })
    : [];

  const wrapperStyles = {
    ...props.style,
    cursor: drag.wrapperStyles.cursor,
    touchAction: drag.wrapperStyles.touchAction,
  };

  const focusData = useFocus(containerRef.current, props);

  useTab(containerRef.current, props, slideRefs);

  return (
    <div
      className={props.className}
      tabIndex={focusData.tabindex}
      ref={containerRef}
      style={wrapperStyles}
      draggable="false"
    >
      {slides}
    </div>
  );
};

export default SliderCore;
