export interface GetDirection {
  (
    index: number,
    next: number,
    total: number,
    continuous: boolean,
    storeDirection?: number
  ): number;
}

export const getDirection: GetDirection = (
  index,
  next,
  total,
  continuous,
  storeDirection = 0
) => {
  let direction;
  if (next === index || total === 1) {
    return 0;
  }

  if (total === 2 && storeDirection) {
    return storeDirection;
  } else {
    if (next < index) {
      if (continuous) {
        if (index - next <= total - (index - next)) {
          direction = -1;
        } else {
          direction = 1;
        }
      } else {
        direction = -1;
      }
    } else {
      if (continuous) {
        if (next - index <= total - (next - index)) {
          direction = 1;
        } else {
          direction = -1;
        }
      } else {
        direction = 1;
      }
    }
  }

  return direction;
};
