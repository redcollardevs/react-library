import type { SliderState, SliderProps } from "../../Slider";
import { useState, useEffect, useRef, useCallback } from "react";
import { useElementResize } from "../../../../shared/resize/useElementResize";
import { simpleTween, Tween } from "../../../utils/animation.utils";
import { JS_ANIMATION_MODE } from "../animations/types";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../../../redux/store";
import {
  sliderSetDirection,
  sliderTo,
} from "../../../../redux/reducers/sliderReducer/actions";

export interface StylesWrapper {
  cursor: string;
  userSelect: string;
  webkitTouchCallout: string;
  touchAction: string;
}

export interface StylesDrag {
  transform: number;
  willChange: string;
  zIndex: number;
}

export interface DragStatus {
  wrapperStyles: StylesWrapper;
  slideStyles: StylesDrag[];
}

export interface Drag {
  (
    currentState: SliderState,
    wrapperElement: HTMLElement | null,
    props: SliderProps
  ): DragStatus;
}

type TouchData = {
  x: number;
  y: number;
  time: number;
};

type Coords = {
  x: number;
  y: number;
};

const defaultDragStyles: StylesDrag[] = [];

const defautlDragWrapperStyles = {
  cursor: "grab",
  userSelect: "none",
  webkitTouchCallout: "none",
  touchAction: "pan-y",
};
/**
 * Hook to enable dragging interactions with a slider (for touchscreens and mouse)
 * @param currentState Current slider state
 * @param wrapperElement Slider wrapper element
 * @param props Slider props
 * @returns Styles for each slider and wrapper element to represent the dragging
 */
export const useDrag: Drag = (currentState, wrapperElement, props) => {
  const dispatch = useDispatch();
  const sliderStoreData = useSelector((state: RootState) => state.slider);
  const sliderState = sliderStoreData?.items.find(
    (item) => item.id === props.id
  );

  const sliderStateIndex = useRef(sliderState ? sliderState.index : 0);
  sliderStateIndex.current = sliderState ? sliderState.index : 0;

  const [slideStyles, setSlideStyles] = useState(defaultDragStyles);
  const slideStylesRef = useRef(defaultDragStyles);
  const [wrapperStyles, setWrapperStyles] = useState(defautlDragWrapperStyles);

  /**
   *  Postition of cursor on drag start
   */
  const start: React.MutableRefObject<TouchData | undefined> =
    useRef(undefined);
  /**
   * Distanse from cursor to the drag start position
   */
  const delta: React.MutableRefObject<Coords | undefined> = useRef(undefined);
  /**
   * Is dragging vertically
   */
  const vertical: React.MutableRefObject<boolean | undefined> =
    useRef(undefined);
  /**
   * Is slider reached it's edge
   */
  const edge: React.MutableRefObject<boolean> = useRef(false);
  /**
   * Is event a touch event
   */
  const touch: React.MutableRefObject<boolean> = useRef(false);
  /**
   * Is dragging starter
   */
  const dragStarted: React.MutableRefObject<boolean> = useRef(false);
  /**
   * Is slider movement started
   */
  const moveStarted: React.MutableRefObject<boolean> = useRef(false);
  /**
   * Was z-index of current slider already set
   */
  const zSet: React.MutableRefObject<boolean> = useRef(false);
  /**
   * Current slide index
   */
  const index: React.MutableRefObject<number> = useRef(0);
  /**
   * Current drag direction
   */
  const direction: React.MutableRefObject<number> = useRef(0);
  /**
   * Next slide index
   */
  const nextIndex: React.MutableRefObject<number | undefined> =
    useRef(undefined);
  /**
   * Movement direction: -1, 1 or 0 if slides shouldn't move
   */
  const moveDirection: React.MutableRefObject<number> = useRef(0);
  /**
   * Animation Tween object for the current slide to return to it's original position
   */
  const returnTweenCurrentSlide: React.MutableRefObject<
    Tween<number> | undefined
  > = useRef(undefined);
  /**
   * Animation Tween object for the next slide to return to it's original position
   */
  const returnTweenNextSlide: React.MutableRefObject<
    Tween<number> | undefined
  > = useRef(undefined);
  /**
   * Distance between the slides of the slider
   */
  const sizeShift: React.MutableRefObject<Number> = useRef(0);
  /**
   * Total number of slider
   */
  const total = currentState.total;
  /**
   * Size of the wrapper element in pixels
   */
  const size = useElementResize(wrapperElement);
  /**
   * Getter for wrapper size in pixels
   */
  const getSize = size.get;

  useEffect(() => {
    let newStyles: StylesDrag[] = [];

    for (let i = slideStyles.length; i < total; i++) {
      newStyles.push({
        transform: 0,
        willChange: "auto",
        zIndex: 0,
      });
    }

    setSlideStyles((prevState) => {
      return prevState.concat(newStyles);
    });
  }, [total, slideStyles.length]);

  const isTouchEvent = useCallback(
    (e: MouseEvent | TouchEvent): e is TouchEvent => {
      return (
        (e as TouchEvent).touches !== undefined &&
        Array.isArray((e as TouchEvent).touches)
      );
    },
    []
  );

  const ontouchstart = useCallback(
    (e: MouseEvent | TouchEvent) => {
      if (isTouchEvent(e)) {
        const touches = e.touches[0];
        start.current = {
          x: touches.pageX,
          y: touches.pageY,
          time: +new Date(),
        };

        touch.current = true;
      } else {
        start.current = {
          x: e.clientX,
          y: e.clientY,
          time: +new Date(),
        };

        touch.current = false;
      }

      index.current = sliderStateIndex.current;

      delta.current = undefined;
      zSet.current = false;
      vertical.current = undefined;
      nextIndex.current = undefined;
      dragStarted.current = true;
      moveStarted.current = true;
      moveDirection.current = 0;

      setWrapperStyles((prevWrapperStyles) => {
        return {
          ...prevWrapperStyles,
          cursor: "grabbing",
        };
      });

      if (returnTweenNextSlide.current) {
        returnTweenNextSlide.current.kill();
      }

      if (returnTweenCurrentSlide.current) {
        returnTweenCurrentSlide.current.kill();
      }
    }, [isTouchEvent]
  );

  const ontouchmove = useCallback(
    (e: MouseEvent | TouchEvent) => {
      if (!dragStarted.current) return;

      let touches: Touch;
      if (isTouchEvent(e)) {
        // @ts-ignore: TS doesn't know about the scale property on TouchEvent
        if (e.touches.length > 1 || (e.scale && e.scale !== 1)) return;
        touches = e.touches[0];

        delta.current = {
          x: touches.pageX - (start.current ? start.current.x : 0),
          y: touches.pageY - (start.current ? start.current.y : 0),
        };

        touch.current = true;
      } else {
        delta.current = {
          x: e.clientX - (start.current ? start.current.x : 0),
          y: e.clientY - (start.current ? start.current.y : 0),
        };

        touch.current = false;
      }

      if (vertical.current === undefined) {
        vertical.current =
          Math.abs(delta.current.x) < Math.abs(delta.current.y);
      }

      if (vertical.current) {
        return;
      }

      /**
       * Distanse from the starting position to the current position
       */
      let dirDelta = delta.current.x;

      if (touch.current) {
        e.preventDefault();
        e.stopPropagation();
      }

      if (!moveStarted.current) {
        // should send an event to stop slider autoscrolling
        moveStarted.current = true;
      }

      /**
       * Temporaty index of the next slide
       */
      let tmpNextIndex;
      if (dirDelta < 0) {
        tmpNextIndex = sliderStateIndex.current + 1;
        if (tmpNextIndex > total - 1) tmpNextIndex = 0;
      } else {
        tmpNextIndex = sliderStateIndex.current - 1;
        if (tmpNextIndex < 0) tmpNextIndex = total - 1;
      }

      if (direction.current !== Math.sign(dirDelta)) {
        direction.current = Math.sign(dirDelta);
        dispatch(sliderSetDirection(props.id, Math.sign(dirDelta)));
      }

      if (
        nextIndex.current !== undefined &&
        tmpNextIndex !== nextIndex.current
      ) {
        setSlideStyles((prevSlideStyles) => {
          slideStylesRef.current = prevSlideStyles.map((s, index) => {
            if (index === nextIndex.current) {
              return { ...s, zIndex: currentState.topZIndex - 1 };
            } else {
              return s;
            }
          });

          return slideStylesRef.current;
        });

        zSet.current = false;
      }

      nextIndex.current = tmpNextIndex;

      if (!zSet.current) {
        zSet.current = true;

        setSlideStyles((prevSlideStyles) => {
          slideStylesRef.current = prevSlideStyles.map((s, index) => {
            if (index === nextIndex.current) {
              return { ...s, zIndex: currentState.topZIndex + 1 };
            } else {
              return s;
            }
          });

          return slideStylesRef.current;
        });
      }

      /**
       * Distance, slides should move to represent the drag
       */
      let move = dirDelta / 3;

      const currentSize = getSize();

      if (move > currentSize.width / 3) {
        move = currentSize.width / 3;
      }
      if (move < -currentSize.width / 3) {
        move = -currentSize.width / 3;
      }

      moveDirection.current = Math.sign(move);

      if (moveDirection.current > 0) {
        sizeShift.current = -currentSize.width;
      } else {
        sizeShift.current = currentSize.width;
      }

      if (!props.continuous) {
        if (dirDelta > 0 && sliderStateIndex.current <= 0) {
          edge.current = true;
        } else if (dirDelta < 0 && sliderStateIndex.current >= total - 1) {
          edge.current = true;
        } else {
          edge.current = false;
        }
      } else {
        edge.current = false;
      }

      if (edge.current) {
        move = move / 4;
      }

      setSlideStyles((prevSlideStyles) => {
        slideStylesRef.current = prevSlideStyles.map((s, index) => {
          if (index === nextIndex.current) {
            return { ...s, transform: move / 2 };
          } else if (index === sliderStateIndex.current) {
            return { ...s, transform: move / 3 };
          } else {
            return s;
          }
        });

        return slideStylesRef.current;
      });
    }, [currentState.topZIndex, dispatch, isTouchEvent, props.continuous, props.id, getSize, total]
  );

  const ontouchend = useCallback(() => {
    /**
     * Duration of the retun animation for dragged elements
     */
    let returnSpeed = 0.25;
    /**
     * Check if slider index should be changed
     */
    let check = !vertical.current && !edge.current;
    /**
     * Distanse from the starting position
     */
    let dirDelta = delta.current ? delta.current.x : 0;
    /**
     * Duration of the drag event to detect if slider was swiped over
     */
    let duration = start.current ? Date.now() - start.current.time : 0;

    const currentSize = getSize();

    check =
      check &&
      ((duration < 250 && Math.abs(dirDelta) > 20) ||
        Math.abs(dirDelta) > currentSize.width / 3.5);

    if (check) {
      returnSpeed = props.speed || 1;
    }

    if (currentState.animationMode === JS_ANIMATION_MODE) {
      if (nextIndex.current !== undefined) {
        returnTweenNextSlide.current = simpleTween<number>(
          {
            from: slideStylesRef.current[nextIndex.current].transform || 0,
            to: 0,
            duration: returnSpeed,
            ease: "ease",
            delay: 0,
          },
          (tick) => {
            setSlideStyles((prevSliderStyles) => {
              slideStylesRef.current = prevSliderStyles.map((s, index) =>
                index === sliderStateIndex.current
                  ? { ...s, transform: tick.value }
                  : s
              );
              return slideStylesRef.current;
            });
          }
        );

        returnTweenCurrentSlide.current = simpleTween<number>(
          {
            from: slideStylesRef.current[sliderStateIndex.current].transform || 0,
            to: 0,
            duration: returnSpeed,
            ease: "ease",
            delay: 0,
          },
          (tick) => {
            setSlideStyles((prevSliderStyles) => {
              slideStylesRef.current = prevSliderStyles.map((s, index) =>
                index === sliderStateIndex.current
                  ? { ...s, transform: tick.value }
                  : s
              );

              return slideStylesRef.current;
            });
          }
        );
      }
    }

    if (check && nextIndex.current !== undefined) {
      dispatch(
        sliderTo(props.id, nextIndex.current, -Math.sign(moveDirection.current))
      );
    }

    zSet.current = false;

    moveDirection.current = 0;
    dragStarted.current = false;
    moveStarted.current = false;
    direction.current = 0;

    setWrapperStyles((prevWrapperStyles) => ({
      ...prevWrapperStyles,
      cursor: "grab",
    }));
  }, [dispatch, props.id, getSize, currentState.animationMode, props.speed]);

  useEffect(() => {
    const ontouchstartCurrent = ontouchstart;
    const ontouchmoveCurrent = ontouchmove;
    const ontouchendCurrent = ontouchend;

    if (props.touch) {
      wrapperElement?.addEventListener("touchstart", ontouchstartCurrent, {
        passive: true,
      });
      wrapperElement?.addEventListener("touchmove", ontouchmoveCurrent, {
        passive: true,
      });
      wrapperElement?.addEventListener("touchend", ontouchendCurrent, {
        passive: true,
      });
    }

    if (props.draggable) {
      wrapperElement?.addEventListener("mousedown", ontouchstartCurrent, {
        passive: true,
      });
      wrapperElement?.addEventListener("mousemove", ontouchmoveCurrent, {
        passive: true,
      });
      wrapperElement?.addEventListener("mouseup", ontouchendCurrent, {
        passive: true,
      });
      wrapperElement?.addEventListener("mouseleave", ontouchendCurrent, {
        passive: false,
      });
    }

    return () => {
      if (props.touch) {
        wrapperElement?.removeEventListener("touchstart", ontouchstartCurrent);
        wrapperElement?.removeEventListener("touchmove", ontouchmoveCurrent);
        wrapperElement?.removeEventListener("touchend", ontouchendCurrent);
      }

      if (props.draggable) {
        wrapperElement?.removeEventListener("mousedown", ontouchstartCurrent);
        wrapperElement?.removeEventListener("mousemove", ontouchmoveCurrent);
        wrapperElement?.removeEventListener("mouseup", ontouchendCurrent);
        wrapperElement?.removeEventListener("mouseleave", ontouchendCurrent);
      }
    };
  }, [
    props.draggable,
    props.touch,
    wrapperElement,
    ontouchstart,
    ontouchmove,
    ontouchend
  ]);

  return {
    wrapperStyles: wrapperStyles,
    slideStyles: slideStyles,
  };
};
