import { useEffect } from "react";
import type { SliderProps } from "../../Slider";
import { useDispatch  } from "react-redux";
import {
  sliderTo,
} from "../../../../redux/reducers/sliderReducer/actions";

interface UseTab {
  (wrapperElement: HTMLElement | null, props: SliderProps, slides: React.RefObject<HTMLDivElement>[]): void;
}

/**
 * If slider has interactive elements inside of it's slides, this hook will change slide to a corresponding one when user puts focus on one of these elements. e.g buttons in slides.
 * @param wrapperElement Wrapper element of the slider
 * @param props Slider props
 * @param slides Array of refs to slides
 */
export const useTab: UseTab = (wrapperElement, props, slides) => {
  const dispatch = useDispatch();

  useEffect(() => {
    /**
     * Checks active element position every time user puses tab (or shift+tab)
     * @param e Keyboard event
     */
    const handleKeyboard = (e: KeyboardEvent) => {
      if (e.key === "Tab") {
        setTimeout(() => {
          const activeElement = document.activeElement;

          if (!wrapperElement || !wrapperElement.contains(activeElement)) {
            return;
          }

          slides.forEach((s, i) => {
            if (s.current && s.current.contains(activeElement)) {
              dispatch(sliderTo(props.id, i, 0));
            }
          });
        }, 0);
      }
    };

    document.addEventListener("keydown", handleKeyboard);

    return () => {
      document.removeEventListener("keydown", handleKeyboard);
    };
  }, [wrapperElement, slides, dispatch, props.id]);
};