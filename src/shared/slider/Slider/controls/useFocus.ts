import type { SliderProps } from "../../Slider";
import { useState, useEffect, useRef } from "react";
import { useDispatch  } from "react-redux";
import {
  sliderPrev,
  sliderNext,
} from "../../../../redux/reducers/sliderReducer/actions";

interface FocusStatus {
  tabindex: number | undefined;
}

interface UseFocus {
  (
    wrapperElement: HTMLElement | null,
    props: SliderProps,
  ): FocusStatus;
}

/**
 * Hook to enable setting a focus on a slider and then contoling it with arrow keys
 * @param wrapperElement Slider wrapper element
 * @param props Slider props
 * @returns Object with tabindex value for the slider
 */
export const useFocus: UseFocus = (wrapperElement, props) => {
  const [tabindex, setTabindex] = useState<number | undefined>(undefined);
  const isFocusOn:React.MutableRefObject<boolean> = useRef(false);
  const dispatch = useDispatch();

  useEffect(() => {
    /**
     * Handler for keydown event. Works when focus is set on the slider
     */
    const handleKeyboard = (e:KeyboardEvent) => {
      if (!isFocusOn.current) return;

      if (e.key === "ArrowLeft") {
        dispatch(sliderPrev(props.id));
      } else if (e.key === "ArrowRight") {
        dispatch(sliderNext(props.id));
      }
    };
  
    /**
     * Handler for focus event on a slider
     */
    const handleFocus = () => {
      isFocusOn.current = true;
    };
  
    /**
     * Handler for blur event on a slider
     */
    const handleBlur = () => {
      isFocusOn.current = false;
    };

    if (props.focusable) {
      wrapperElement?.addEventListener("focus", handleFocus);
      wrapperElement?.addEventListener("blur", handleBlur);
      document.addEventListener('keydown', handleKeyboard);
    }

    setTabindex(0);

    return () => {
      if (props.focusable) {
        wrapperElement?.removeEventListener("focus", handleFocus);
        wrapperElement?.removeEventListener("blur", handleBlur);
        document.removeEventListener('keydown', handleKeyboard);
      }

      setTabindex(undefined);
    }
  }, [wrapperElement, props.focusable, props.id, dispatch]);

  return {
    tabindex: tabindex,
  };
};