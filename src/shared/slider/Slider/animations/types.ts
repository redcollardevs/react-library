export const JS_ANIMATION_MODE = "JS";
export const CSS_ANIMATION_MODE = "CSS";

export const DEFAULT_ANIMATION = "DEFAULT_ANIMATION";

export const animationTypes = {
    DEFAULT_ANIMATION
}