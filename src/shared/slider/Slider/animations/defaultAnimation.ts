import React, { useState, useEffect, useRef } from "react";
import { useElementResize } from "../../../resize/useElementResize";
import type { StylesAnimation, Animation } from "./interface";
import { getDirection } from "../common/getDirection";
import { simpleTween } from "../../../utils/animation.utils";
import { animationTypes, JS_ANIMATION_MODE } from "./types";
import { useSelector } from "react-redux";
import { RootState } from "../../../../redux/store";
import { setStateThreshold } from "../../../utils/react.utils";

const defaultState: StylesAnimation[] = [];

export const useDefaultAnimation: Animation = (
  currentState,
  wrapperElement,
  props
) => {
  const size = useElementResize(wrapperElement);
  const [styles, setStyles] = useState(defaultState);
  const currentIndex: React.MutableRefObject<number> = useRef(
    currentState.index
  );

  const sliderStoreData = useSelector((state: RootState) => state.slider);
  const sliderState = sliderStoreData?.items.find(
    (item) => item.id === props.id
  );
  
  const total = sliderState ? sliderState.total : 0;

  useEffect(() => {
    let newStyles: StylesAnimation[] = [];

    for (let i = 0; i < total; i++) {
      const direction = getDirection(
        i,
        sliderState?.index || 0,
        total,
        props.continuous || true,
        sliderState?.direction
      );

      let opacity = 1;
      let transform = 0;

      if (i !== sliderState?.index || 0) {
        opacity = 0;
        transform = -direction * size.width;
      }

      newStyles.push({
        transform: transform,
        opacity: opacity,
        willChange: "none",
      });
    }

    setStateThreshold<StylesAnimation[]>(setStyles, () => {
      return newStyles;
    }, 0.001);
  }, [total, props.continuous, sliderState?.direction, sliderState?.index, size.width]);

  if (!sliderState) {
    return defaultState;
  }

  const newIndex = sliderState.index;
  const prevIndex = currentIndex.current;

  if (sliderState.index !== currentIndex.current) {
    currentIndex.current = sliderState.index;

    const direction = sliderState.direction ? sliderState.direction : getDirection(
      prevIndex,
      newIndex,
      total,
      props.continuous || true
    );

    simpleTween(
      {
        from: 0,
        to: 1,
        duration: props.speed || 1,
        ease: "ease",
        delay: 0,
      },
      (tick) => {
        setStateThreshold<StylesAnimation[]>(setStyles, (prevState) => {
          return prevState.map((style, index) => {
            if (index === prevIndex) {
              return {
                opacity: 1 - tick.easeProgress,
                transform: -direction * size.width * tick.value,
                willChange: "transform",
              };
            } else {
              return style;
            }
          });
        }, 0.01);
      },
      () => {
        setStateThreshold<StylesAnimation[]>(setStyles, (prevState) => {
          return prevState.map((style, index) => {
            if (index === prevIndex) {
              return {
                opacity: style.opacity,
                transform: style.transform,
                willChange: "auto",
              };
            } else {
              return style;
            }
          });
        }, 0.01);
      }
    );

    simpleTween(
      {
        from: 1,
        to: 0,
        duration: props.speed || 1,
        delay: 0,
        ease: "ease",
      },
      (tick) => {
        setStateThreshold<StylesAnimation[]>(setStyles, (prevState) => {
          return prevState.map((style, index) => {
            if (index === newIndex) {
              return {
                opacity: tick.easeProgress,
                transform: direction * size.width * tick.value,
                willChange: "transform",
              };
            } else {
              return style;
            }
          });
        }, 0.01);
      },
      () => {
        setStateThreshold<StylesAnimation[]>(setStyles, (prevState) => {
          return prevState.map((style, index) => {
            if (index === newIndex) {
              return {
                opacity: style.opacity,
                transform: style.transform,
                willChange: "auto",
              };
            } else {
              return style;
            }
          });
        }, 0.01);
      }
    );
  }

  return styles.map((slide) => {
    return {
      opacity: slide.opacity,
      transform: slide.transform,
      willChange: slide.willChange,
    };
  });
};

export const defaultAnimationName = animationTypes.DEFAULT_ANIMATION;
export const defaultAnimationMode = JS_ANIMATION_MODE;