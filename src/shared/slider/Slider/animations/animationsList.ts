import { Animation } from "./interface";
import { useDefaultAnimation, defaultAnimationMode, defaultAnimationName } from "./defaultAnimation";

type AnimationObject = {
    useAnimation:Animation,
    mode: string,
    name: string 
}

type AnimationList = {
    [index: string]:AnimationObject
}

const defaultAnimetion:AnimationObject = {
    useAnimation: useDefaultAnimation,
    mode: defaultAnimationMode,
    name: defaultAnimationName
}

export const animationList:AnimationList = {
    [defaultAnimationName]: defaultAnimetion,
}