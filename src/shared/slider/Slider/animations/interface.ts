import type { SliderState, SliderProps } from "../../Slider";

export interface StylesAnimation {
  willChange?: string;
  transitionProperty?: string;
  transitionDuration?: number;
  transitionDelay?: number;
  transitionTimingFunction?: string;
  transform?: number;
  opacity?: number;
  zIndex?: number;
}

export interface Animation {
  (
    currentState: SliderState,
    wrapperElement: HTMLElement | null,
    props: SliderProps
  ): StylesAnimation[]
}