import { SliderProps, SliderState } from "./Slider";
import { DEFAULT_ANIMATION } from "./Slider/animations/types";
import { animationList } from "./Slider/animations/animationsList";

interface SliderDefaultProps extends SliderProps {
  speed: number;
  continuous: boolean;
  focusable: boolean;
  touch: boolean;
  draggable: boolean;
  resize: boolean;
  leftSlideClassName: string;
  rightSlideClassName: string;
  centerSlideClassName: string;
  animation: string;
}

export const sliderDefaultProps: SliderDefaultProps = {
  id: "",
  speed: 1,
  continuous: true,
  focusable: true,
  touch: true,
  draggable: true,
  resize: false,
  leftSlideClassName: "slide__left",
  rightSlideClassName: "slide__right",
  centerSlideClassName: "slide__center",
  animation: animationList[DEFAULT_ANIMATION].name,
};

export const sliderDefaultState: SliderState = {
  index: 0,
  total: 0,
  direction: "horizontal",
  topZIndex: 0,
  animationMode: animationList[DEFAULT_ANIMATION].mode,
  animationName: animationList[DEFAULT_ANIMATION].name
};
