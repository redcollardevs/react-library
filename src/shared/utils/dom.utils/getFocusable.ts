export const getFocusable = (element?: HTMLElement):Array<HTMLElement> => {
  if (!element) element = document.documentElement;

  const nodeList = element.querySelectorAll(
    "a, button, input, textarea, select, details, [tabindex]"
  );
  const nodeArray = Array.prototype.slice.call(nodeList);

  return nodeArray.filter((el) => {
    return !el.hasAttribute("disabled");
  });
};
