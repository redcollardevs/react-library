import React from "react";

export const useCombinedRefs = (...refs : React.MutableRefObject<any>[] | Array<(ref: any) => void>) => {
    const targetRef = React.useRef()
  
    React.useEffect(() => {
      refs.forEach((ref:React.MutableRefObject<any> | ((ref: any) => void)) => {
        if (!ref) return
  
        if (typeof ref === 'function') {
          ref(targetRef.current)
        } else {
          ref.current = targetRef.current
        }
      })
    }, [refs])
  
    return targetRef
  }