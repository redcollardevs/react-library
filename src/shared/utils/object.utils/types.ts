export type defaultObject = {
    [index: string]: unknown;
};

export type numbersObject = {
    [index: string]: number;
};

export const isNumbersObject = (p: any): p is numbersObject => {
    if (typeof p !== "object") return false;
    for (let key in p) {
        if (typeof p[key] !== "number") {
            return false;
        }
    }
    return true;
}

export interface ObjectMath<ObjectType extends numbersObject> {
    (obj1: ObjectType, obj2: ObjectType): ObjectType
}

export interface ObjectMathMix<ObjectType extends numbersObject> {
    (obj1: ObjectType, obj2: ObjectType, mixValue: number): ObjectType
}