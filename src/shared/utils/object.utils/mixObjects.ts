import { mix } from "../math.utils/mix";
import { numbersObject, ObjectMathMix } from "./types";

export const mixObjects:ObjectMathMix<numbersObject> = (
    obj1: numbersObject,
    obj2: numbersObject,
    mixValue: number
) => {
    const newObject: numbersObject = {};

    Object.keys(obj1).forEach((key) => {
        newObject[key] = mix(
            obj1[key],
            obj2[key],
            mixValue
        );
    });

    return newObject;
};
