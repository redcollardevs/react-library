import { numbersObject, ObjectMath } from "./types";

export const subObjects: ObjectMath<numbersObject> = (obj1, obj2) => {
    const newObject: numbersObject = {};

    Object.keys(obj1).forEach((key) => {
        newObject[key] = obj1[key] - obj2[key];
    });

    return newObject;
};
