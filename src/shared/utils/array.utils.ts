import { addArrays } from "./array.utils/addArrays";
import { arraysEqual } from "./array.utils/arraysEqual";
import { mixArrays } from "./array.utils/mixArrays";
import { multiplyArrays } from "./array.utils/multiplyArrays";
import { subArrays } from "./array.utils/subArrays";

export {
    addArrays,
    arraysEqual,
    mixArrays,
    multiplyArrays,
    subArrays
}