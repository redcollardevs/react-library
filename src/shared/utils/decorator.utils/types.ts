export type FCDecoratorI = <T extends Function>(Component: React.ComponentType<T>) => T | void;
