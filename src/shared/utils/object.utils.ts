import { addObjects } from "./object.utils/addObjects";
import { mixObjects } from "./object.utils/mixObjects";
import { multiplyObjects } from "./object.utils/multiplyObjects";
import { subObjects } from "./object.utils/subObjects";
import { deepCopy } from "./object.utils/deepCopy";

export {
    addObjects,
    mixObjects,
    multiplyObjects,
    subObjects,
    deepCopy
}