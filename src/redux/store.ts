import { configureStore } from "@reduxjs/toolkit";
import { sliderReducer } from "./reducers/sliderReducer/reducer";
import { modalReducer } from "./reducers/modalReducer/reducer";
import { scrollReducer } from "./reducers/scrollReducer/reducer";

export const store = configureStore({
  reducer: {
	  slider: sliderReducer,
    modal: modalReducer,
    scroll: scrollReducer
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
