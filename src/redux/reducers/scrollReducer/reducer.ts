import { Reducer } from "react";
import type { AnyAction } from "redux";
import {
  SCROLL_LOCK,
  SCROLL_UNLOCK,
  SCROLL_LOCK_FREEZE,
  SCROLL_LOCK_UNFREEZE
} from "./types";

interface ScrollState {
  locked: boolean,
  frozen: boolean
}

let initialState: ScrollState = {
  locked: false,
  frozen: false
};

export const scrollReducer: Reducer<ScrollState | undefined, AnyAction> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case SCROLL_LOCK: return state.frozen ? state : state.locked ? state : {...state, locked: true};
    case SCROLL_UNLOCK: return state.frozen ? state : state.locked ? {...state, locked: false} : state;
    case SCROLL_LOCK_FREEZE: return state.frozen ? state : {...state, frozen: true};
    case SCROLL_LOCK_UNFREEZE: return state.frozen ? {...state, frozen: false}: state;
    default: return state;
  }
}