export const SLIDER_ADD = "SLIDER_ADD";
export const SLIDER_REMOVE = "SLIDER_REMOVE";
export const SLIDER_UPDATE = "SLIDER_UPDATE";
export const SLIDER_NEXT = "SLIDER_NEXT";
export const SLIDER_PREV = "SLIDER_PREV";
export const SLIDER_TO = "SLIDER_TO";
export const SLIDER_SET_DIRECTION = "SLIDER_SET_DIRECTION";

export type SliderFullPayload = {
    id: string;
    index: number;
    total: number;
    continuous: boolean;
    direction: number;
}

export type SliderIdPayload = {
    id: string;
}

export type SliderIndexPayload = {
    id: string;
    index: number;
    direction: number;
}

export type SliderDirectionPayload = {
    id: string;
    direction: number;
}

export interface SliderAdd {
    type: typeof SLIDER_ADD,
    payload: SliderFullPayload
}

export interface SliderRemove {
    type: typeof SLIDER_REMOVE,
    payload: SliderIdPayload
}

export interface SliderUpdate {
    type: typeof SLIDER_UPDATE,
    payload: SliderFullPayload
}

export interface SliderNext {
    type: typeof SLIDER_NEXT,
    payload: SliderIdPayload
}

export interface SliderPrev {
    type: typeof SLIDER_PREV,
    payload: SliderIdPayload
}

export interface SliderTo {
    type: typeof SLIDER_TO,
    payload: SliderIndexPayload
}

export interface SliderSetDirection {
    type: typeof SLIDER_SET_DIRECTION,
    payload: SliderDirectionPayload
}